# sint_dashboard

> Frontend of the SINT car rental project

This is a Vue.js 2 client for the exercise project in SINT at the UAS Technikum Wien in the summer term 2018.

The client access a car rental web service and the google maps static API. A lot of this stuff here has to be understood as a proof of concept for the use of web services. We did not implement a fully productive environment, so there is no persistent database in the background and we have no state-of-the art security features built in. When playing around with this be aware that your credentials should not be used anywhere in case you are using the webservice over HTTP.

To clone this project use:

```
git clone https://gitlab.com/jackieklaura/SINT_dashboard.git
```

Afterwards you can either run the client locally in a development mode or build a deployment version that you can put on any static webspace. See the section _Build Setup_ for more on this. First you have to make sure to fulfill the requirements and do a little configuration.

## Requirements

Beyond the requirements of Vue.js itself, there are no additional requirements. So if you have `npm` installed you'll just be fine with following the steps in the _Build Setup_ section. If you want to know more about installing or optimising Vue.js take a look at their [installation guide](https://vuejs.org/v2/guide/installation.html). `npm` is the _Node.js package manager_ which is installed with _Node.js_. Take a look at [Get npm!](https://www.npmjs.com/get-npm) or head over directly to [nodejs.org](https://nodejs.org). In case you are using a Linux distribution you might want to [install Node.js via your package manager](https://nodejs.org/en/download/package-manager/).

## Configuration

When you cloned the repository you will find a `config` folder inside the project root. Open the `prod.env.js` file and edit the following lines:
```
RENTAL_API: '"<put the link to your car rental web service here>"'
GOOGLE_MAPS_API_KEY: '"<put you google maps api key here>"'
```

You can read up on the [Google Static Maps API](https://developers.google.com/maps/documentation/static-maps/intro) if you want to find out more how this service gets used. If you just want to quickly create an API key look at Google's [Get API Key and Signature](https://developers.google.com/maps/documentation/static-maps/get-api-key) help page.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Deployment of the production build

If you used `npm run build` to produce a production build, you probably want to deploy it to any static webspace available to you. For that you only need to copy the contents of the `dist` folder to the the root directory of your webspace (or any subfolder you want to deploy it in).

If you have set up an AWS S3 bucket that acts as static webspace and also have set up your AWS CLI, deployment is as easy as:
```
aws s3 sync dist s3://your-unique-bucket-identifier
```
