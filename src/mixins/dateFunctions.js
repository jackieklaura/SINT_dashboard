var day = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
]

var month = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

export default {
  methods: {
    apiDate: function (date) {
      var d = new Date(date)
      return this.leadingZero(d.getDate()) + '-' + this.leadingZero(d.getMonth() + 1) + '-' + d.getFullYear()
    },
    apiNow: function () {
      var d = new Date()
      return this.leadingZero(d.getDate()) + '-' + this.leadingZero(d.getMonth() + 1) + '-' + d.getFullYear()
    },
    prettyDate: function (date) {
      var d = new Date(date)
      var dstring = ''
      dstring += day[d.getDay()] + ', '
      dstring += this.leadingZero(d.getDate()) + '. '
      dstring += month[d.getMonth()] + ' '
      dstring += d.getFullYear()
      return dstring
    },
    leadingZero: function (num) {
      if (num < 10) return '0' + num
      else return num.toString()
    }
  }
}
